# Xbtsx Gateway Service

Xbtsx is a gateway service built on the BDEX Exchange. A gateway service is responsible for moving cryptocurrencies to and from the BDEX Exchange. They support a wide range of popular assets. You can easily identify those supported by XbtsX because they are prefixed with the word XBDEXX.*. For example XBDEXX.STH, XBDEXX.POST etc.

## Website
[XBDEX.io](https://xbts.io)

## Support
- [Telegram Chat](https://t.me/xbtsio)
